//h1 { color:red}
//.parrafito { ... }
// #pid { ... }

console.log("Hello, world!!");

/*const h1 = document.querySelector('h1');
const p = document.querySelector('p');
const parrafito = document.querySelector('.parrafito');
const pid = document.querySelector('#pid');
const input = document.querySelector('input');*/

/* Otras formas de lectura */

const h1 = document.querySelector('h1');
const p = document.querySelectorAll('p');
const parrafito = document.getElementsByClassName('.parrafito');
const pid = document.getElementById('pid');
const input = document.querySelector('input');

console.log(h1);
console.log(input.value); // acceder al valor del input

// nos muestra el objeto con esas propiedades
console.log({
    h1, p, parrafito, pid, input
});


//h1.innerHTML = 'Cordero <br> Lion' //Convierte a HTML
h1.innerText = 'Cordero <br> lion';// Convierte a texto, permite dar una proteccion basica.
//console.log(h1.getAttribute('pantalla')) // lee cualquier atributo asignado en el elemento.
console.log(h1.getAttribute('class')); // obtener los atributos.
h1.setAttribute('class','rojo');// forma para modificar los atributos.

h1.classList.add('purple'); //Agregar una clase nueva
//h1.classList.remove('rojo') //Elimina clases que no requiera
//h1.classList.toggle('rojo') //Cuando ya existen eventos de javascript agregar o quitar  clases dependiendo de una acción
//h1.classList.contains('rojo') //Devuelve true o false si tiene o no la clase.

//Entrar a modificar el value
input.value = "777"

const img = document.createElement('img'); // creamos una imagen
img.setAttribute('src','https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSQHfLL5gubHWomK2KvZt2vzUEMKu5Dcz5urQ&usqp=CAU'); //Le damos los atributos y modificaciones correspondientes
console.log(img);//mostramos resultados en la consola
pid.innerHTML =""; //limpia lo que tenemos en el parrafo con id
pid.appendChild(img); // agrega la img al DOM, or appendChild también la agrega

 